import { Injectable } from '@angular/core';
import {Router} from "@angular/router";

export class AuthModel{
  constructor(
    public username: string,
    public password: string
  ){}
}

const users = [
  new AuthModel('admin','terragon123'),
];

@Injectable()
export class AuthService {

  constructor(
    private _router: Router){}

  logout() {
    localStorage.removeItem("user");
    this._router.navigate(['Login']);
  }

  login(user){
    const authenticatedUser = users.find(u => u.username === user.username);
    if (authenticatedUser && authenticatedUser.password === user.password){
      localStorage.setItem("user", JSON.stringify(authenticatedUser));
      this._router.navigate(['/dashboard']);
      return true;
    }
    return false;

  }

  checkCredentials(){
    if (localStorage.getItem("user") === null){
      this._router.navigate(['Login']);
    }
  }
}
