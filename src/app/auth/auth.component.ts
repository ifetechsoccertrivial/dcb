import {Component, OnInit, Input} from '@angular/core';
import {AuthService, AuthModel} from "./auth.service";
import {FormBuilder, FormGroup, Form} from '@angular/forms';
import {Router} from "@angular/router";


@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  // providers: [AuthService],
})
export class AuthComponent implements OnInit {

  public errorMsg = '';
  public isLoggedIn = false;

  authForm  : FormGroup;
  private loggedIn: number;

  constructor(fb: FormBuilder, private router: Router) {
    this.authForm = fb.group(
      {
        'username': '',
        'password': ''
      }
    )
  }

  submitForm(value: any):void{
    const username = value.username;
    const password = value.password;
    if (username == "admin" && password == "terragon2017"){
        this.router.navigate(['/dashboard']);
        localStorage.setItem('isLoggedIn', true.toString())
    }else{
      this.errorMsg = "Invalid Username/Password";
      localStorage.setItem('isLoggedIn', false.toString())
    }
  }





  ngOnInit() {
  }

}
