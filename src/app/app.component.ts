import { Component } from '@angular/core';
import {AuthComponent} from "./auth/auth.component";

@Component({
  // tslint:disable-next-line
  selector: 'body',
  template: '<router-outlet></router-outlet>',

})
export class AppComponent { }
